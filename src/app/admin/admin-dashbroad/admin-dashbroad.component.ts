import { HttpClient } from '@angular/common/http';
import { ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin-dashbroad',
  templateUrl: './admin-dashbroad.component.html',
  styleUrls: ['./admin-dashbroad.component.less']
})
export class AdminDashbroadComponent implements OnInit {
  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
   gridApi:any;
   gridColumnApi:any;

   columnDefs:any;
   defaultColDef:any;
   rowData:any;

  constructor(private http: HttpClient,private modal: NzModalService, private viewContainerRef: ViewContainerRef) {
    this.columnDefs = [
      { field: 'athlete' },
      {
        field: 'age',
        width: 80,
      },
      { field: 'country' },
      {
        field: 'year',
        width: 90,
      },
      { field: 'date' },
      { field: 'sport' },
      { field: 'gold' },
      { field: 'silver' },
      { field: 'bronze' },
    ];
    this.defaultColDef = {
      width: 150,
      sortable: true,
      resizable: true,
      filter: true,
    };
  }
ngOnInit(): void {
  //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
  //Add 'implements OnInit' to the class.

}
  onGridReady(params:any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.http
      .get('https://www.ag-grid.com/example-assets/olympic-winners.json')
      .subscribe((data) => {
        var differentHeights = [40, 80, 120, 200];
        // data.forEach((dataItem:any, index:any) =>{
        //   dataItem.rowHeight = differentHeights[index % 4];
        // });
        this.rowData = data
      });
  }

  getRowHeight(params:any) {
    return params.data.rowHeight;
  }
  onCellDoubleClicked($event: any){
    this.modal.create({
      nzTitle: 'Modal Title',
      nzContent: 'string, will close after 1 sec',
      nzClosable: false,
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000))
    });
  }
}
