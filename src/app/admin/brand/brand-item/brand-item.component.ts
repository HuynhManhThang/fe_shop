import { HttpClient } from '@angular/common/http';
import { Input, Output, ViewContainerRef, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observer } from 'rxjs';
import { Observable } from 'rxjs';
import { BrandsService } from 'src/app/core/services/brand.service';
import { map, finalize } from "rxjs/operators";
import { AngularFireStorage } from '@angular/fire/storage';
const getBase64 = (file: File): Promise<string | ArrayBuffer | null> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
@Component({
  selector: 'app-brand-item',
  templateUrl: './brand-item.component.html',
  styleUrls: ['./brand-item.component.less']
})
export class BrandItemComponent implements OnInit {

  @Input() type = 'add';
  @Input() item: any;
  @Input() isVisible = false;
  @Input() option: any;
  @Output() eventEmmit = new EventEmitter<any>();
  value: any;
  isVisibleTop = false;
  fb: any;
  downloadURL!: Observable<string>;
  fileList: NzUploadFile[] = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  loading = false;
  avatarUrl?: string;
  data: any
  constructor(private http: HttpClient,
    private modal: NzModalService,
    private msg: NzMessageService,
    private viewContainerRef: ViewContainerRef,
    private storage: AngularFireStorage,
    private brandsService: BrandsService) {

  }
  ngOnInit(): void {
    this.loading= this.fb !=null ? false :true
  }
  public initData(data: any, type: any = null, option: any = {}): void {
    if (type === 'ADD') {
    } else if (type === 'UPDATE') {
      this.value = data.name,
        this.type = type;
      this.data = data;
     this.fb= this.previewImage=data.images
    }
  }
  handleOkMiddle(): void {
    this.isVisible = false;
    if (this.fb != null && this.value != null) {
      if (this.type === "ADD") {
        this.brandsService.insert({
          name: this.value,
          images: this.fb
        }).subscribe((data: any) => {
          this.eventEmmit.emit({ type: 'success' });
          this.msg.success("Thêm mới không thành công")
        }, (err: any) => {
          this.eventEmmit.emit({ type: 'err' });
          console.log(err);
          this.msg.success("Thêm mới không thành công, Vui lòng điền lại thông tin")
        })
      } else {
        this.brandsService.update({
          id: '' + this.data.id,
          name: this.value,
          images: this.fb
        }).subscribe((data: any) => {
          this.eventEmmit.emit({ type: 'success' });
          this.msg.success("Thêm mới không thành công")
        }, (err: any) => {

          this.msg.success("Cập nhật không thành công, Vui lòng điền lại thông tin")
        })
      }
    }
  }

  handleCancelMiddle(): void {
    this.isVisible = false;
    this.eventEmmit.emit({ type: 'cancel' });
  }

  handlePreview = async (file: NzUploadFile) => {

    if (!file.url && !file.preview) {
      file.preview = this.fb;
    }
    this.previewImage = this.fb;
    this.previewVisible = true;
  };
  transformFile = (file: NzUploadFile) =>
    new Observable((observer: Observer<Blob>) => {
      this.loading=false
      const reader = new FileReader();
      reader.readAsDataURL(file as any);
      reader.onload = () => {
        var n = Date.now();
        const fileUp = file;
        const filePath = `RoomsImages/${n}`;
        const fileRef = this.storage.ref(filePath);
        const task = this.storage.upload(`RoomsImages/${n}`, fileUp);
        task
          .snapshotChanges()
          .pipe(
            finalize(() => {
              this.downloadURL = fileRef.getDownloadURL();
              this.downloadURL.subscribe(url => {
                if (url) {
                  this.fb = url;
                  this.previewImage = url;
                  // this.msg.success("Tải ảnh thành công")
                  this.loading=true
                }
              },(err:any)=>{
                this.msg.success("Tải ảnh không thành công ,vui lòng tải lại!")
              });
            })
          )
          .subscribe(url => {
            if (url) {
              // console.log("url firebase=",url)
            }
          });
      };
    });
}
