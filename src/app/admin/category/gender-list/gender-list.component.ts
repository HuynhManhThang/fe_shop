import { HttpClient } from '@angular/common/http';
import { ViewChild, ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { EXCEL_STYLES_DEFAULT, OVERLAY_LOADING_TEMPLATE } from 'src/app/share/contants';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GendersService } from 'src/app/core/services/gender.service';
@Component({
  selector: 'app-gender-list',
  templateUrl: './gender-list.component.html',
  styleUrls: ['./gender-list.component.less']
})
export class GenderListComponent implements OnInit {



  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  gridApi: any;
  gridColumnApi: any;
  buttonName: boolean = true
  columnDefs: any;
  defaultColDef: any;
  rowData: any;
  frameworkComponents: any;
  excelStyles: any;
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate = "Không có dữ liệu , vui lòng liên hệ quản trị viên!";
  constructor(private http: HttpClient, private msg: NzMessageService, private datePipe: DatePipe, private modal: NzModalService, private viewContainerRef: ViewContainerRef, private service: GendersService) {
    this.columnDefs = [

      {
        field: 'name',
        headerName: 'Tên kiểu dáng',
        resizable: true
      },
      {
        // headerName: 'Thao tác',
        cellRenderer: 'btnCellRender',
        width: 150,
        cellRendererParams: {
          editClicked: (item: any) => this.onEdit({ data: item }),
          deleteClicked: (item: any) => this.onDeleteItem(item),
        },
      }
    ];
    this.defaultColDef = {
      sortable: true,
      flex: 1,
      minWidth: 100,
      filter: true,
      resizable: true,

    };
    this.frameworkComponents = {
      btnCellRender: BtnCellRenderComponent,

    };
    this.excelStyles = [...EXCEL_STYLES_DEFAULT];
  }
  ngOnInit(): void {}


  onDeleteItem(item: any = null): void {

    let selectedRows = this.gridApi.getSelectedRows();
    if (item !== null) {
      selectedRows = [];
      selectedRows.push(item);
    }
    this.service.delete(item.id).subscribe((data: any) => {
      this.msg.success("Đã xóa thành công")
      this.onGridReady()
    }, (err: any) => {
      this.msg.success("Vui lòng thử lại")
      console.log(err);

    })
  }
  onGridReady(params?: any) {
    if (params != null) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    this.service.findAll().subscribe((data: any) => {
      // console.table(data);
      for (const item of data.data) {
        // item.infoGrantAccess = true;
        item.editGrantAccess = true;
        item.deleteGrantAccess = true;
      }
      this.rowData = data.data;
      this.gridApi.hideOverlay();
    }, (err: any) => {
      console.log(err);
      this.gridApi.showNoRowsOverlay();

    })
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }
  idType: any
  onEdit($event: any) {

    let data = $event.data
    this.valueColor = data.name
    this.idType = data.id
    this.buttonName = false
  }
  valueColor: any
  onAdd() {
    if (this.valueColor != null || this.valueColor != undefined || this.valueColor != 0) {
      if (this.buttonName) {
        this.service.insert({ name: this.valueColor }).subscribe((data: any) => {
          this.msg.success("Thêm mới kiểu dáng  thành công")
          this.onGridReady()
        }, (err: any) => {
          this.msg.success("Vui lòng thử lại")
          console.log(err);

        })
      } else {
        this.service.update({ id: this.idType, name: this.valueColor }).subscribe((data: any) => {
          this.msg.success("Cập nhật kiểu dáng thành công")
          this.buttonName=true
          this.onGridReady()
        }, (err: any) => {
          this.msg.success("Vui lòng thử lại")
          console.log(err);

        })
      }

    }
  }
  changAction() {
    this.buttonName = !this.buttonName
  }

}
