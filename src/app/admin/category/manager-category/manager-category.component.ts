import { HttpClient } from '@angular/common/http';
import { ViewChild, ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common';
import { BrandsService } from 'src/app/core/services/brand.service';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { EXCEL_STYLES_DEFAULT, OVERLAY_LOADING_TEMPLATE } from 'src/app/share/contants';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
  selector: 'app-manager-category',
  templateUrl: './manager-category.component.html',
  styleUrls: ['./manager-category.component.less']
})
export class ManagerCategoryComponent implements OnInit {



  modalBrand: any = {
    type: '',
    item: {},
    isShow: false,
    option: {},
  };
  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  gridApi: any;
  gridColumnApi: any;

  columnDefs: any;
  defaultColDef: any;
  rowData: any;
  frameworkComponents: any;
  excelStyles: any;
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate="Không có dữ liệu , vui lòng liên hệ quản trị viên!";
  constructor(private http: HttpClient, private msg: NzMessageService, private datePipe: DatePipe, private modal: NzModalService, private viewContainerRef: ViewContainerRef, private brandsService: BrandsService) {

  }
  ngOnInit(): void {}


  onDeleteItem(item: any = null): void {

    let selectedRows = this.gridApi.getSelectedRows();
    if (item !== null) {
      selectedRows = [];
      selectedRows.push(item);
    }
    this.brandsService.delete(item.id).subscribe((data: any) => {
      this.msg.success("Đã xóa thành công")
      this.onGridReady()
    }, (err: any) => {
      this.msg.success("Vui lòng thử lại")
      console.log(err);

    })

  }
  onGridReady(params?: any) {
    if (params != null) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    this.brandsService.findAll().subscribe((data: any) => {
      // console.table(data);
      for (const item of data.data) {
        // item.infoGrantAccess = true;
        item.editGrantAccess = true;
        item.deleteGrantAccess = true;
      }
      this.rowData = data.data;
      this.gridApi.hideOverlay();
    }, (err: any) => {
      console.log(err);
      this.gridApi.showNoRowsOverlay();

    })
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }


  onModalEventEmmit(event: any): void {
    this.modalBrand.isShow = false;
    if (event.type === 'success') {
      // this.initGridData();
      this.onGridReady()
    }
  }
}
