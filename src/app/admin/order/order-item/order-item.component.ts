import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'src/app/core/services/order.service';
import { CartService } from 'src/app/core/services/Cart.service';
import { ProductService } from 'src/app/core/services/products.service';
@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.less']
})
export class OrderItemComponent implements OnInit, OnDestroy {

  index = 0;
  initLoading = true; // bug
  loadingMore = false;
  data: any;
  list: any;
  totalAmmount: any;
  isLoading: boolean = false
  toggle: boolean = true
  paramID = this.route.snapshot.params.id
  dataPRO: any;
  check_cart_or_order: any
  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private msg: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService,
    private productService: ProductService,
    private cartService: CartService,
  ) {
    this.check_cart_or_order = this.route.snapshot.routeConfig?.path == "order-add" ? 1 : 0;
    // console.log(this.check_cart_or_order);

    this.data = {
      status_bill: 0
    }
  }
  getbillid: any = "";
  getcartid: any = "";
  dataAdd: any
  ngOnInit(): void {
    if (this.check_cart_or_order == 0) {
      this.getById()
    } else {
      this.isLoading = this.initLoading = false;
      this.orderService.insertoffline().subscribe((res: any) => {
        this.dataAdd = res.data
        this.getbillid = res.data.id;
        this.getcartid = res.data.cart.id;
      }, (err: any) => {
        console.log(err);
      })
    }
    this.infoForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      fullname: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
    });
    this.bidndingListPro()
  }
  updateProfile() {
    this.isLoading = this.initLoading = true
    this.cartService.updateProfile({ bill_id: this.check_cart_or_order == 0 ? this.paramID : this.getbillid, ...this.infoForm.value }).subscribe((res: any) => {
      this.getById()
    }, (err: any) => {
      console.log(err);
    }, () => {
      this.isLoading = this.initLoading = false
    });
  }
  getById() {
    const id = this.check_cart_or_order == 0 ? this.paramID : this.getbillid
    this.orderService.findById(id)
      .pipe(catchError(() => of({ results: [] })))
      .subscribe((res: any) => {
        const data = res.data
        this.infoForm.patchValue({
          email: data?.email,
          fullname: data?.fullname,
          phone: data?.phone,
          address: data?.address,
        })
        this.data = res.data
        this.list = res.data?.cart?.productCarts
        this.initLoading = false;
        this.getTotalPrice()
      }, (err: any) => {
        console.log(err);

      })
  }
  onIndexChange(event: number): void {
    this.index = event;
  }
  infoForm!: FormGroup;

  submitForm(): void {
    for (const i in this.infoForm.controls) {
      if (this.infoForm.controls.hasOwnProperty(i)) {
        this.infoForm.controls[i].markAsDirty();
        this.infoForm.controls[i].updateValueAndValidity();
      }
    }
  }

  getTotalPrice() {
    let total = 0;
    this.list?.map((item: any) => {
      total += item.total_item;
    });
    this.totalAmmount = total
  }
  remove(item: any): void {
    this.initLoading = true
    this.cartService.removeProductFromCart(item.id).subscribe((res: any) => {
      this.getById()
    }, (err: any) => {
      console.log(err);

    }, () => {
      this.initLoading = false
    });
  }

  Continnue(id: any, action: any) {
    ++action
    this.isLoading = true
    this.orderService.update(id, action)
      .pipe(catchError(() => of({ results: [] })))
      .subscribe((res: any) => {
        this.getById()
      }, (err: any) => {
        console.log(err);
      }, () => {
        this.isLoading = false
      })
  }

  updatePro(product: any) {
    const data = { productCart_id: product.id, quantity: product.quantity }
    this.cartService.update(data).subscribe((res: any) => {
      this.getById()
    }, (err: any) => {
      console.log(err);
    }, () => {
    });
  }
  inputValue?: string;
  options: any;
  allData: any
  onInput(e: Event): void {
    const value = (e.target as HTMLInputElement).value;
    this.dataPRO = this.allData
    this.options = this.dataPRO.filter((user: any) => user.name.includes(this.inputValue))
  }
  bidndingListPro() {
    this.productService.findAll().subscribe((data: any) => {
      this.dataPRO = data.data
      this.allData = data.data
    })
  }
  addPro(item: any) {
    const data = { bill_id: this.check_cart_or_order == 0 ? this.paramID : this.getbillid, product_id: item }
    this.initLoading = true
    this.cartService.addProBill(data).subscribe((res: any) => {
      this.getById()
    }, (err: any) => {
      console.log(err);
    }, () => {
      this.initLoading = false
    });
  }
  ngOnDestroy(){
    debugger
    if (this.check_cart_or_order == 1) {
      const dt = this.dataAdd
      if (!dt.fullname && !dt.email && !dt.phone) {
        confirm("Vui lòng nhập thông tin người mua đơn hàng")
      }
    }


  }
}
