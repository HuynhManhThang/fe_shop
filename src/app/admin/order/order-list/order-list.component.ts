import { filter } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ViewChild, ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { EXCEL_STYLES_DEFAULT, OVERLAY_LOADING_TEMPLATE, OVERLAY_NOROW_TEMPLATE } from 'src/app/share/contants';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BrandItemComponent } from '../../brand/brand-item/brand-item.component';
import { OrderService } from 'src/app/core/services/order.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.less']
})
export class OrderListComponent implements OnInit {


  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  gridApi: any;
  gridColumnApi: any;
  radioValue: any = '5';
  columnDefs: any;
  defaultColDef: any;
  rowData: any;
  frameworkComponents: any;
  excelStyles: any;
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate = "Không có dữ liệu , vui lòng liên hệ quản trị viên!";
  allData: any;
  filterBy: any;
  constructor(private http: HttpClient,
    private msg: NzMessageService,
    private datePipe: DatePipe,
    private modal: NzModalService,
    private viewContainerRef: ViewContainerRef,
    private services: OrderService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.columnDefs = [
      {
        headerName: 'Họ và Tên',
        field: 'fullname',
        resizable: true,

      },
      {
        field: 'phone',
        headerName: 'Số điện thoại',
        resizable: true
      },
      {
        headerName: 'Địa chỉ',
        field: 'address',
        resizable: true,
      },
      {
        headerName: 'Hình thức thanh toán',
        field: 'payment',
        resizable: true,

      },
      {
        headerName: 'Ngày đặt hàng',
        field: 'create_at',
        resizable: true,
        cellRenderer: (params: any) => {
          // console.log(params);
          return datePipe.transform(params.data.create_at, 'dd/MM/yyy-hh:mm:ss')
        }
      },
      {
        headerName: 'Trạng thái',
        field: 'status_bill',
        resizable: true,
        cellRenderer: (params: any): String | any => {
          let data: any
          if (params.data.status_bill == 0) {
            data = "Chờ xác nhận";
            return `<p style="color:blue">${data}</p>`
          } else if (params.data.status_bill == 1) {
            data = "Đang đóng gói";
            return `<p style="color:gray">${data}</p>`
          } else if (params.data.status_bill == 2) {
            data = "Đang giao hàng";
            return `<p style="color:green">${data}</p>`
          }
          else if (params.data.status_bill == 3) {
            data = "Đã thanh toán";
            return `<p style="color:red">${data}</p>`
          } else if (params.data.status_bill == 4) {
            data = "Đã hủy";
            return `<p style="color:black">${data}</p>`
          }
        }
      },
      {
        cellRenderer: 'btnCellRender',
        width: 150,
        cellRendererParams: {
          editClicked: (item: any) => this.router.navigateByUrl("/order/" + item.id),
          deleteClicked: (item: any) => this.onDeleteItem(item),
          printClicked: (item: any) => this.router.navigateByUrl("/order/" + item.id),
        },
      }
    ];
    this.defaultColDef = {
      sortable: true,
      flex: 1,
      minWidth: 100,
      filter: true,
      resizable: true,

    };
    this.frameworkComponents = {
      btnCellRender: BtnCellRenderComponent,

    };
    this.excelStyles = [...EXCEL_STYLES_DEFAULT];
  }
  ngOnInit(): void { }


  onDeleteItem(item: any = null): void {

    let selectedRows = this.gridApi.getSelectedRows();
    if (item !== null) {
      selectedRows = [];
      selectedRows.push(item);
    }
    this.services.delete(item.id).subscribe((data: any) => {
      this.msg.success("Đã xóa thành công")
      this.onGridReady()
    }, (err: any) => {
      this.msg.success("Vui lòng thử lại")
      console.log(err);

    })

  }
  changeStatus() {
    if (this.radioValue != 5) {
      this.rowData = this.allData
      this.rowData = this.rowData.filter((item: any) => {
        return item.status_bill == this.radioValue
      })
    } else {
      this.rowData = this.allData
    }

  }
  onGridReady(params?: any) {
    if (params != null) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    this.services.findAll().subscribe((data: any) => {
      // console.table(data);
      for (const item of data.data) {
        // item.infoGrantAccess = true;
        item.editGrantAccess = true;
        item.printGrantAccess = true;
        if (item.status_bill<3) {
          item.deleteGrantAccess = true;
        }
      }
      this.rowData = data.data;
      this.allData = data.data;
      this.gridApi.hideOverlay();
    }, (err: any) => {
      console.log(err);
      this.gridApi.showNoRowsOverlay();

    })
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }
  onCellDoubleClicked($event: any) {
    this.router.navigateByUrl("/order/" + $event.data.id)
  }
  filter() {
    debugger
    this.rowData = this.allData
    this.rowData = [...this.rowData.filter((user: any) =>{
      if (user.fullname||user.phone||user.address) {
      return user.fullname.includes(this.filterBy) || user.phone.includes(this.filterBy) || user.address.includes(this.filterBy)
      }
     }
       )]
  }
}


