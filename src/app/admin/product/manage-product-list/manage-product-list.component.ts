import { HttpClient } from '@angular/common/http';
import { ViewChild, ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common';
import { BrandsService } from 'src/app/core/services/brand.service';
import { ManagerProductItemComponent } from '../manager-product-item/manager-product-item.component';
import { ProductService } from 'src/app/core/services/products.service';
import { EXCEL_STYLES_DEFAULT, OVERLAY_LOADING_TEMPLATE } from 'src/app/share/contants';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
  selector: 'app-manage-product-list',
  templateUrl: './manage-product-list.component.html',
  styleUrls: ['./manage-product-list.component.less']
})
export class ManageProductListComponent implements OnInit {



  @ViewChild(ManagerProductItemComponent, { static: false }) initBrandModal!: { initData: (arg0: {}, arg1: string) => void };
  modalBrand: any = {
    type: '',
    item: {},
    isShow: false,
    option: {},
  };
  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  gridApi: any;
  gridColumnApi: any;

  columnDefs: any;
  defaultColDef: any;
  rowData: any;
  frameworkComponents: any;
  excelStyles: any;
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate="Không có dữ liệu , vui lòng liên hệ quản trị viên!";
  filterBy: any;
  allData: any;
  constructor( private productService:ProductService,private http: HttpClient, private msg: NzMessageService, private datePipe: DatePipe, private modal: NzModalService, private viewContainerRef: ViewContainerRef, private brandsService: BrandsService) {
    this.columnDefs = [
      {
        headerName: 'Hình ảnh',
        field: 'images',
        resizable: true,
        cellRenderer: (params: any) => {
          return `<img style="height:40px; margin:0 auto" onerror="this.src='../../../../assets/image/brand.svg'" src="${params.value}" />`
        }
      },
      {
        field: 'name',
        headerName: 'Tên sản phẩm',
        resizable: true
      }, {
        field: "price_new",
        headerName: 'Giá Giảm',
        resizable: true,
      }, {
        field: "price_old",
        headerName: 'Giá Cũ',
        resizable: true,
      },{
        headerName: 'Hiệu',
        field: 'brand.name',
        resizable: true,
      },

      {
        headerName: 'Màu',
        field: 'color.name',
        resizable: true,
      },{
        headerName: 'Ngày tạo',
        field: 'create_at',
        resizable: true,
        cellRenderer: (params: any) => {
          // console.log(params);
          return datePipe.transform(params.data.create_at, 'dd/MM/yyy')
        }
      },
      {
        // headerName: 'Thao tác',
        cellRenderer: 'btnCellRender',
        width:150,
        cellRendererParams: {
          editClicked: (item: any) => this.onCellDoubleClicked({ data: item }),
          deleteClicked: (item: any) => this.onDeleteItem(item),
          // deleteClicked: (item: any) => this.onDeleteItem(item),
        },
      }
    ];
    this.defaultColDef = {
      sortable: true,
      flex: 1,
      minWidth: 100,
      filter: true,
      resizable: true,

    };
    this.frameworkComponents = {
      btnCellRender: BtnCellRenderComponent,

    };
    this.excelStyles = [...EXCEL_STYLES_DEFAULT];
  }
  ngOnInit(): void {}


  onDeleteItem(item: any = null): void {

    let selectedRows = this.gridApi.getSelectedRows();
    if (item !== null) {
      selectedRows = [];
      selectedRows.push(item);
    }
    this.productService.delete(item.id).subscribe((data: any) => {
      this.msg.success("Đã xóa thành công")
      this.onGridReady()
    }, (err: any) => {
      this.msg.success("Vui lòng thử lại")
      console.log(err);

    })

  }
  onGridReady(params?: any) {
    if (params != null) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    this.productService.findAll().subscribe((data: any) => {
      // console.table(data);
      for (const item of data.data) {
        // item.infoGrantAccess = true;
        item.editGrantAccess = true;
        item.deleteGrantAccess = true;
      }
      this.rowData = data.data;
      this.allData = data.data;
    // console.log( this.rowData);

      this.gridApi.hideOverlay();
    }, (err: any) => {
      console.log(err);
      this.gridApi.showNoRowsOverlay();

    })
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }
  onCellDoubleClicked($event: any) {
    this.modalBrand = {
      type: 'UPDATE',
      isShow: true,
      option: {},
    };
    let data = $event.data
    this.productService.findById(data.id).subscribe((res: any) => {
      console.log(res);
      if (res.data) {
          this.initBrandModal.initData(res.data, 'UPDATE');
      }
    }, (err) => {
      console.log(err);
    })
  }
  onAdd() {
    this.modalBrand = {
      type: 'ADD',
      isShow: true,
      option: {},
    };
    this.initBrandModal.initData("", 'ADD');
  }
  onModalEventEmmit(event: any): void {
    this.modalBrand.isShow = false;
    if (event.type === 'success') {
      // this.initGridData();
      this.onGridReady()
    }
  }
  filter() {
    debugger
    this.rowData = this.allData
    this.rowData = [...this.rowData.filter((prod: any) =>{
      if (prod.name) {
      return prod.name.includes(this.filterBy)
      }
     }
       )]
  }
}
