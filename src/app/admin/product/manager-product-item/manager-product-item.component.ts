import { HttpClient } from '@angular/common/http';
import { Input, Output, ViewContainerRef, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observer } from 'rxjs';
import { Observable } from 'rxjs';
import { BrandsService } from 'src/app/core/services/brand.service';
import { map, finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ColorsService } from 'src/app/core/services/color.service';
import { GendersService } from 'src/app/core/services/gender.service';
import { TypesService } from 'src/app/core/services/type.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/products.service';
const getBase64 = (file: File): Promise<string | ArrayBuffer | null> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
@Component({
  selector: 'app-manager-product-item',
  templateUrl: './manager-product-item.component.html',
  styleUrls: ['./manager-product-item.component.less'],
})
export class ManagerProductItemComponent implements OnInit {
  @Input() type = 'add';
  @Input() item: any;
  @Input() isVisible = false;
  @Input() option: any;
  @Output() eventEmmit = new EventEmitter<any>();
  value: any;
  isVisibleTop = false;
  fb: any;
  downloadURL!: Observable<string>;
  fileList: NzUploadFile[] = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  loading = false;
  avatarUrl?: string;
  data: any;
  dataBinding: any;
  productForm!: FormGroup;
  percent: number = 0;
  soGiam: number | any;
  htmlContent = '';
  title:any
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [['bold']],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
  };
  constructor(
    private http: HttpClient,
    private modal: NzModalService,
    private msg: NzMessageService,
    private storage: AngularFireStorage,
    private brandsService: BrandsService,
    private colorServices: ColorsService,
    private genderServices: GendersService,
    private typeServices: TypesService,
    private fbuild: FormBuilder,
    private productSevices: ProductService
  ) {}
  ngOnInit(): void {
    this.productForm = this.fbuild.group({
      id:[null],
      name: [null, [Validators.required]],
      image: [null],
      images: [null],
      description: [null],
      gender_id: [null, [Validators.required]],
      price_old: [null, [Validators.required]],
      price_new: [null],
      color_id: [null, [Validators.required]],
      brand_id: [null, [Validators.required]],
      type_id: [null, [Validators.required]],
      soGiam: [null],
      percent: [1],
    });
  }
  remove(id:any){
    this.fb=this.fb.filter((item:any)=>{
     return item!=id
    })
  }
  genderChange(value: string): void {
    this.productForm
      .get('note')!
      .setValue(value === 'male' ? 'Hi, man!' : 'Hi, lady!');
  }
  brands: any;
  colors: any;
  genders: any;
  types: any;
  bindingData() {
    this.brandsService.findAll().subscribe((data: any) => {
      this.brands = data.data;
    });
    this.colorServices.findAll().subscribe((data: any) => {
      this.colors = data.data;
    });
    this.genderServices.findAll().subscribe((data: any) => {
      this.genders = data.data;
    });
    this.typeServices.findAll().subscribe((data: any) => {
      this.types = data.data;
    });
  }
  public initData(data: any, type: any = null, option: any = {}): void {
    this.bindingData();
    if (type === 'ADD') {
      this.title="Thêm Mới Sản Phẩm"
    } else if (type === 'UPDATE') {
      (this.value = data.name), (this.type = type);
      this.data = data;
        this.title="Cập nhật Sản Phẩm"
      // const [brand,colorWatch,name,description,images,price_old,price_new,typeWatch,genderWatch]=data
      this.productForm.controls['price_new'].setValue(data.price_new);
      this.fb=data.images.split(",");
      this.htmlContent=data.description;
      this.productForm.controls['price_old'].setValue(data.price_old);
      this.productForm.controls['soGiam'].setValue((data.price_old-data.price_new));
      this.productForm.controls['percent'].setValue(1);
      this.productForm.controls['name'].setValue(data.name);
      this.productForm.controls['color_id'].setValue(''+data.colorWatch.id);
      this.productForm.controls['brand_id'].setValue(''+data.brand.id);
      this.productForm.controls['type_id'].setValue(''+data.typeWatch.id);
      this.productForm.controls['gender_id'].setValue(''+data.genderWatch.id);
      this.productForm.controls['price_new'].setValue(data.price_new);
      this.productForm.controls['id'].setValue(data.id);
    }
  }
  resetFrom(){
    this.productForm.reset()
    this.fb='';
    this.htmlContent=''
  }
  handleOkMiddle(): void {
    // this.isVisible = false;
    for (const i in this.productForm.controls) {
      if (this.productForm.controls.hasOwnProperty(i)) {
        this.productForm.controls[i].markAsDirty();
        this.productForm.controls[i].updateValueAndValidity();
      }
    }
    let total;
    if (this.productForm.value.percent == 0) {
      total =
        +this.productForm.value.price_old -
        +this.productForm.value.price_old *
          (this.productForm.value.soGiam / 100);
    } else {
      total = +this.productForm.value.price_old - this.productForm.value.soGiam;
    }
    this.productForm.controls['price_new'].setValue(total);
    this.productForm.controls['images'].setValue(this.fb);
    this.productForm.controls['description'].setValue(this.htmlContent);
    if (this.fb) {
        this.productForm.controls['images'].setValue(this.fb.join(','));
         if (this.type === 'ADD') {
      this.productSevices.insert(this.productForm.value).subscribe(
        (data: any) => {
          this.eventEmmit.emit({ type: 'success' });
          this.msg.success('Thêm mới thành công');
          this.resetFrom()
        },
        (err: any) => {
          this.eventEmmit.emit({ type: 'err' });
          console.log(err);
          this.msg.success(
            'Thêm mới không thành công, Vui lòng điền lại thông tin'
          );
        }
      );
    } else {
      this.productSevices
        .update(this.productForm.value)
        .subscribe(
          (data: any) => {
            this.eventEmmit.emit({ type: 'success' });
            this.msg.success('Cập nhật thành công');
            this.resetFrom()
          },
          (err: any) => {
            this.msg.success(
              'Cập nhật không thành công, Vui lòng điền lại thông tin'
            );
          }
        );
    }
    }else{
      this.msg.error("Vui lòng thêm ảnh!")
    }


  }

  handleCancelMiddle(): void {
    this.isVisible = false;
    this.resetFrom()
    this.eventEmmit.emit({ type: 'cancel' });
  }

  handlePreview = async (file: NzUploadFile) => {
    ;
    if (!file.url && !file.preview) {
      file.preview = this.fb;
    }
    this.previewImage = this.fb;
    this.previewVisible = true;
  };
  count: number = 0;
  a: any;
  transformFile = (file: NzUploadFile) =>
    new Observable((observer: Observer<Blob>) => {
      const reader = new FileReader();
      reader.readAsDataURL(file as any);
      reader.onload = () => {
        var n = Date.now();
        const fileUp = file;
        const filePath = `RoomsImages/${n}`;
        const fileRef = this.storage.ref(filePath);
        const task = this.storage.upload(`RoomsImages/${n}`, fileUp);
        task
          .snapshotChanges()
          .pipe(
            finalize(() => {
              this.downloadURL = fileRef.getDownloadURL();
              this.downloadURL.subscribe(
                (url) => {
                  if (url) {
                    if (this.fb == null) {
                      this.fb = [url];
                    } else {
                      this.fb = this.fb.concat(url);
                    }
                  }
                },
                (err: any) => {
                  this.msg.success(
                    'Tải ảnh không thành công ,vui lòng tải lại!'
                  );
                }
              );
            })
          )
          .subscribe((url) => {
            if (url) {
              // console.log('url firebase=', url);
            }
          });
      };
    });
}
