import { HttpClient } from '@angular/common/http';
import { ViewChild, ViewContainerRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common';
import { BrandsService } from 'src/app/core/services/brand.service';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { EXCEL_STYLES_DEFAULT, OVERLAY_LOADING_TEMPLATE, OVERLAY_NOROW_TEMPLATE } from 'src/app/share/contants';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BrandItemComponent } from '../../brand/brand-item/brand-item.component';
import { AccountService } from 'src/app/core/services';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.less']
})
export class UserListComponent implements OnInit {

  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  gridApi: any;
  gridColumnApi: any;

  columnDefs: any;
  defaultColDef: any;
  rowData: any;
  frameworkComponents: any;
  excelStyles: any;
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate="Không có dữ liệu , vui lòng liên hệ quản trị viên!";
  constructor(private http: HttpClient, private msg: NzMessageService, private datePipe: DatePipe, private modal: NzModalService, private viewContainerRef: ViewContainerRef, private brandsService: AccountService) {
    this.columnDefs = [
      {
        headerName: 'Họ và Tên',
        field: 'fullname',
        resizable: true
      },
      {
        field: 'phone',
        headerName: 'Số điện thoại',
        resizable: true
      },
      {
        headerName: 'Email',
        field: 'email',
        resizable: true
      },
      {
        headerName: 'Địa chỉ',
        field: 'address',
        resizable: true
      },
    ];
    this.defaultColDef = {
      sortable: true,
      flex: 1,
      minWidth: 100,
      filter: true,
      resizable: true,

    };
    this.frameworkComponents = {
      btnCellRender: BtnCellRenderComponent,

    };
    this.excelStyles = [...EXCEL_STYLES_DEFAULT];
  }
  ngOnInit(): void {}


  onDeleteItem(item: any = null): void {

    let selectedRows = this.gridApi.getSelectedRows();
    if (item !== null) {
      selectedRows = [];
      selectedRows.push(item);
    }
    this.brandsService.delete(item.id).subscribe((data: any) => {
      this.msg.success("Đã xóa thành công")
      this.onGridReady()
    }, (err: any) => {
      this.msg.success("Vui lòng thử lại")
      console.log(err);

    })

  }
  onGridReady(params?: any) {
    if (params != null) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    }

    this.brandsService.getAll().subscribe((data: any) => {
      this.rowData = data;
      // console.log( this.rowData);

      this.gridApi.hideOverlay();
    }, (err: any) => {
      console.log(err);
      this.gridApi.showNoRowsOverlay();

    })
  }

  getRowHeight(params: any) {
    return params.data.rowHeight;
  }
  onCellDoubleClicked($event: any) {

  }

}
