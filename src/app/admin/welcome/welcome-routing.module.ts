import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashbroadComponent } from '../admin-dashbroad/admin-dashbroad.component';
import { BrandListComponent } from '../brand/brand-list/brand-list.component';
import { ManagerCategoryComponent } from '../category/manager-category/manager-category.component';
import { ManageProductListComponent } from '../product/manage-product-list/manage-product-list.component';
import { TypeListComponent } from '../category/type-list/type-list.component';
import { WelcomeComponent } from './welcome.component';
import { OrderListComponent } from '../order/order-list/order-list.component';
import { UserListComponent } from '../user/user-list/user-list.component';
import { OrderItemComponent } from '../order/order-item/order-item.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent , children: [
    { path: '', redirectTo: '/dashbroad', pathMatch: 'full' },
    // { path: 'dashboard', component: AdminDashbroadComponent },
    { path: 'brands', component: BrandListComponent },
    { path: 'types', component: TypeListComponent },
    { path: 'manager-category', component: ManagerCategoryComponent },
    { path: 'manager-product', component: ManageProductListComponent },
    { path: 'customer', component: UserListComponent },
    { path: 'order', component: OrderListComponent },
    { path: 'order/:id', component: OrderItemComponent },
    { path: 'order-add', component: OrderItemComponent },
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
