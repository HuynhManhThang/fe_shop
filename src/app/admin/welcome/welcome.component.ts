import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountService } from 'src/app/core/services';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit {
  isCollapsed = false;

  user: any
  constructor(
    private acc:AccountService
  ) {
  }

  ngOnInit(): void {
    let a = JSON.parse("" + localStorage.getItem('user-login'))
    this.user = a.data
  }
  logout() {
    this.acc.logout()
  }
}


