import { NgModule } from '@angular/core';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { IconsProviderModule } from 'src/app/icons-provider.module';

import { WelcomeRoutingModule } from './welcome-routing.module';

import { WelcomeComponent } from './welcome.component';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { AgGridModule } from 'ag-grid-angular';
import { CommonModule } from '@angular/common';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { AdminDashbroadComponent } from '../admin-dashbroad/admin-dashbroad.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BrandItemComponent } from '../brand/brand-item/brand-item.component';
import { BrandListComponent } from '../brand/brand-list/brand-list.component';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzMessageServiceModule } from 'ng-zorro-antd/message';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { AngularFireModule } from '@angular/fire';
import { DatePipe } from '@angular/common';
import {
  AngularFireStorageModule,
} from "@angular/fire/storage";
import { environment } from 'src/environments/environment';
import { TypeListComponent } from '../category/type-list/type-list.component';
import { GenderListComponent } from '../category/gender-list/gender-list.component';
import { ManageProductListComponent } from '../product/manage-product-list/manage-product-list.component';
import { ManagerProductItemComponent } from '../product/manager-product-item/manager-product-item.component';
import { BtnCellRenderComponent } from 'src/app/share/btn-cell-render/btn-cell-render.component';
import { ManagerCategoryComponent } from '../category/manager-category/manager-category.component';
import { ColorListComponent } from '../category/color-list/color-list.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { OrderListComponent } from '../order/order-list/order-list.component';
import { OrderItemComponent } from '../order/order-item/order-item.component';
import { UserListComponent } from '../user/user-list/user-list.component';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
@NgModule({
  imports: [
    AgGridModule.withComponents([]),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WelcomeRoutingModule,
    NzLayoutModule,
    IconsProviderModule,
    NzMenuModule,
    NzAvatarModule,
    NzDropDownModule,
    NzCardModule,
    NzGridModule,
    NzStatisticModule,
    NzModalModule,
    NzBreadCrumbModule,
    NzButtonModule,
    NzInputModule,
    NzMessageServiceModule,
    NzUploadModule,
    NzPopconfirmModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    // AngularFirestoreModule,
    AngularFireStorageModule,
    AngularEditorModule,
    NzSelectModule,
    NzPopoverModule,
    NzFormModule,
    NzStepsModule,
    NzListModule,
    NzSkeletonModule,
    NzBadgeModule,
    NzRadioModule,
    NzAutocompleteModule,
    NzEmptyModule

  ],
  declarations: [
    WelcomeComponent,
    AdminDashbroadComponent,
    BrandItemComponent,
    BrandListComponent,
    TypeListComponent,
    GenderListComponent,
    ManageProductListComponent,
    ManagerProductItemComponent,
    BtnCellRenderComponent,
    ManagerCategoryComponent,
    ColorListComponent,
    OrderListComponent,
    OrderItemComponent,
    UserListComponent,
  ],
  exports: [WelcomeComponent],
  providers: [DatePipe],
})
export class WelcomeModule {}
