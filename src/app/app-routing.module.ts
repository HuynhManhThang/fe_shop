import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/_helpers';
import { NotFoundComponent } from './share/not-found/not-found.component';


const routes: Routes = [
  { path: '', loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule) },
  { path: '', canActivate: [AuthGuard], loadChildren: () => import('./admin/welcome/welcome.module').then(m => m.WelcomeModule) },
  { path: '**',component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
