import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad } from '@angular/router';
import { AccountService } from '../services';
import { Route } from '@angular/compiler/src/core';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate,CanLoad {
  constructor(
    private router: Router,
    private accountService: AccountService,
  ) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let user = this.accountService?.userValue;
    if (user?.data?.role?.name == "ROLE_ADMIN") {
      return true;
    }
    alert("Tài khoản không có quyền truy cập!")
    this.router.navigateByUrl('/index');
    return false;
  }
}
