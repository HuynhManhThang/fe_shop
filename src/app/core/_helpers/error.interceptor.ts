import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AccountService } from '../services';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private accountService: AccountService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 403) {
              localStorage.setItem("isLogin", ""+0)
              alert("Tài khoản hết hạn truy cập, vui lòng đăng nhập lại!")
                this.accountService.logout();
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}
