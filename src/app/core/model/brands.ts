export interface Brand {
    id?: number|string;
    name: string;
    image?: string;
    images?: string;
  }
  