export interface Color {
  id?: number|string;
  name: string;
  image?: string;
  images?: string;
}
