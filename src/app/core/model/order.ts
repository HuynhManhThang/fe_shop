export interface Order {
  id?: number | string;
  fullname: string;
  phone?: number;
  address?: string;
  total_money?: number;
  status_bill?: number;
  color_id?: string;
  brand_id?: string;
  type_id?: string;
}
