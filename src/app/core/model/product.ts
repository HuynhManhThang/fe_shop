export interface Product {
  id?: number | string;
  name: string;
  image?: string;
  images?: string;
  description?: string;
  gender_id?: string;
  price_old?: number|any;
  price_new?: number|any;
  color_id?: string;
  brand_id?: string;
  type_id?: string;
}
