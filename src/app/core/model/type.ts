export interface Type {
  id?: number|string;
  name: string;
  create_at?: any;
  update_at?: any;
}
