import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core"
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs";
import { Subject } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  constructor(
    protected http: HttpClient
  ) {
  }
  public products = new BehaviorSubject('');
  private sort = new BehaviorSubject('');
  sharedData = this.sort.asObservable();
  getProducts = this.products.asObservable();
  // getProducts(): Observable<any> {
  //   return this.products.asObservable();
  // }

  setSort(item: any) {
    this.sort.next(item)
  }
  dataview: any
  // Add single product to the cart
  addProductToCart(product: any) {
    const addCart = {
      product_id: product.id,
      product: product,
      cart_id: localStorage.getItem("cart_id"),
      quantity: 1,
    }
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    this.http.post<any>(environment.BASE_URL + 'v1/api/cart/addtocart', addCart, { headers }).subscribe((res: any) => {
      if (res.data) {
        this.dataview = res.data ? res.data.productCarts : []
        this.products.next(this.dataview);
      }
    }, (err: any) => {
      console.log(err);
    })

  }

  // Remove single product from the cart
  removeProductFromCart(id: any) {
    this.products.next("");
    return this.http.delete<any>(environment.BASE_URL + 'v1/api/cart/deletetocart/' + id);
  }

  /**
  * Update specific object into DB
  * @param Order the object to be updated
  * @returns gets the response
  */
  public update(product: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    this.products.next("");
    return this.http.put<any>(environment.BASE_URL + 'v1/api/cart/updatetocart', product);
  }
  // Calculate total price on item added to the cart
  /**
    * Update specific object into DB
    * @param Order the object to be updated
    * @returns gets the response
    */
  public addProBill(product: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<any>(environment.BASE_URL + 'v1/api/bill/updateProduct', product);
  }
  /**
    * Update specific object into DB
    * @param Order the object to be updated
    * @returns gets the response
    */
  public updateProfile(product: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<any>(environment.BASE_URL + 'v1/api/bill/updateProfile', product);
  }

  /**
     * Insert the data
     * @param data the object containing the data to be inserted
     * @returns gets the response
     */
  public addCart(model: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<any>(environment.BASE_URL + 'v1/api/cart/addtocart', model, { headers });
  }
  /**
    * Insert the data
    * @param data the object containing the data to be inserted
    * @returns gets the response
    */
  public Bill(model: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    this.products.next("");
    return this.http.post<any>(environment.BASE_URL + 'v1/api/bill', model, { headers });
  }
  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public createCart(model: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<any>(environment.BASE_URL + 'v1/api/cart', model, { headers });
  }

  /**
  * Find an object by its identifier
  * @param id the object identifier
  * @returns gets the object found
  */
  public checkUserId(id: any): Observable<any> {
    return this.http.get<any>(environment.BASE_URL + 'v1/api/cart/checkByUserId/' + id);
  }

  /**
  * Find an object by its identifier
  * @param id the object identifier
  * @returns gets the object found
  */
  public getCartById(id: any): Observable<any> {
    return this.http.get<any>(environment.BASE_URL + 'v1/api/cart/' + id);
  }
}
