import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../model';
import { environment } from 'src/environments/environment';


@Injectable({ providedIn: 'root' })
export class AccountService {
  private userSubject: BehaviorSubject<any>;
  public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    const u = localStorage.getItem('user-login')
    this.userSubject = new BehaviorSubject<User>(JSON.parse("" + u));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): any {
    return this.userSubject.value;
  }

  login(data: any) {
    return this.http.post<User>(`${environment.BASE_URL}v1/api/login`, data)
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user-login', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }
  changepass(data: any) {
    return this.http.post<User>(`${environment.BASE_URL}v1/api/changePassword`, data)
      .pipe(map(user => {
        // localStorage.setItem('user-login', JSON.stringify(user));
        // this.userSubject.next(user);
        return user;
      }));
  }
  updateProfile(data: any) {
    return this.http.put<any>(`${environment.BASE_URL}v1/api/user`, data)
      .pipe(map(user => {
        // localStorage.setItem('user-login', JSON.stringify(user));
        // this.userSubject.next(user);
        return user;
      }));
  }
  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user-login');
    localStorage.removeItem('user_id');
    localStorage.removeItem('cart_id');
    localStorage.removeItem('isLogin');
    this.userSubject.next(null);
    window.location.reload()
  }

  register(user: any) {
    return this.http.post(`${environment.BASE_URL}v1/api/register`, user);
  }

  getAll() {
    return this.http.get<any>(`${environment.BASE_URL}v1/api/user`);
  }

  getById(id: string) {
    return this.http.get<User>(`${environment.BASE_URL}/users/${id}`);
  }

  update(id: any, params: any) {
    return this.http.put(`${environment.BASE_URL}/users/${id}`, params)
      .pipe(map(x => {
        // update stored user if the logged in user updated their own record
        if (id == this.userValue.id) {
          // update local storage
          const user = { ...this.userValue, ...params };
          localStorage.setItem('user', JSON.stringify(user));

          // publish updated user to subscribers
          this.userSubject.next(user);
        }
        return x;
      }));
  }

  delete(id: string) {
    return this.http.delete(`${environment.BASE_URL}/users/${id}`)
      .pipe(map(x => {
        // auto logout if the logged in user deleted their own record
        if (id == this.userValue.id) {
          this.logout();
        }
        return x;
      }));
  }
}
