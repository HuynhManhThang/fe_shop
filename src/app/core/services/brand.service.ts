import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Brand } from '../model/brands';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
  })
export class BrandsService {

  constructor(protected http: HttpClient) {
  }

  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Brand> {
    return this.http.get<Brand>(environment.BASE_URL + 'v1/api/brand' + id);
  }

  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?:any): Observable<Brand[]> {
    return this.http.get<Brand[]>(environment.BASE_URL+'v1/api/brand', {params});
  }

  /**
   * Delete an object by its identifier field
   * @param id the object identifier
   * @returns gets the response
   */
  public delete(id:any): Observable<Brand> {
    return this.http.delete<Brand>(environment.BASE_URL +'v1/api/brand/'+ id);
  }

  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public insert(data: Brand): Observable<Brand> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<Brand>(environment.BASE_URL+'v1/api/brand', data, {headers});
  }

  /**
   * Update specific object into DB
   * @param Brand the object to be updated
   * @returns gets the response
   */
  public update(Brand: Brand): Observable<Brand> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Brand>(environment.BASE_URL+ 'v1/api/brand', Brand);
  }
}
