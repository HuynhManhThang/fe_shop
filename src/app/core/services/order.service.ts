import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { Order } from '../model/order';

@Injectable({
    providedIn: 'root',
  })
export class OrderService {

  constructor(protected http: HttpClient) {
  }

  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Order> {
    return this.http.get<Order>(environment.BASE_URL + 'v1/api/bill/' + id);
  }
public myorder(id: any): Observable<Order> {
    return this.http.get<Order>(environment.BASE_URL + 'v1/api/getbill/' + id);
  }
  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?:any): Observable<Order[]> {
    return this.http.get<Order[]>(environment.BASE_URL+'v1/api/bill', {params});
  }

  /**
   * Delete an object by its identifier field
   * @param id the object identifier
   * @returns gets the response
   */
  public delete(id:any): Observable<Order> {
    return this.http.delete<Order>(environment.BASE_URL +'v1/api/bill/'+ id);
  }

  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public insert(data: Order): Observable<Order> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<Order>(environment.BASE_URL+'v1/api/bill', data, {headers});
  }
  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public insertoffline(): Observable<Order> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<Order>(environment.BASE_URL+'v1/api/creatbill',{headers});
  }
  /**
   * Update specific object into DB
   * @param Order the object to be updated
   * @returns gets the response
   */
  public update(id: any,action:any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    let url:any
    if (action==1) {
       url="v1/api/bill/packBill/"
    }else if (action==2) {
     url="v1/api/bill/deliveryBill/"
    }else if (action==3) {
      url="v1/api/bill/continueBill/"
    }else if (action==4) {
      url="v1/api/bill/doneBill/"
    }
    return this.http.put(environment.BASE_URL  + url + id, id);
  }
}
