import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../model/product';

@Injectable({
    providedIn: 'root',
  })
export class ProductService {

  constructor(protected http: HttpClient) {
  }

  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Product> {
    return this.http.get<Product>(environment.BASE_URL + 'v1/api/product/' + id);
  }

  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?:any): Observable<Product[]> {
    return this.http.get<Product[]>(environment.BASE_URL+'v1/api/product', {params});
  }

  /**
   * Delete an object by its identifier field
   * @param id the object identifier
   * @returns gets the response
   */
  public delete(id:any): Observable<Product> {
    return this.http.delete<Product>(environment.BASE_URL +'v1/api/product/'+ id);
  }

  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public insert(data: Product): Observable<Product> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<Product>(environment.BASE_URL+'v1/api/product', data, {headers});
  }

  /**
   * Update specific object into DB
   * @param product the object to be updated
   * @returns gets the response
   */
  public update(product: Product): Observable<Product> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Product>(environment.BASE_URL+ 'v1/api/product', product);
  }
}
