import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Type } from '../model/type';
const api = 'v1/api/type'
@Injectable({
  providedIn: 'root',
})
export class TypesService {

  constructor(protected http: HttpClient) {
  }

  /**
   * Find an object by its identifier
   * @param id the object identifier
   * @returns gets the object found
   */
  public findById(id: any): Observable<Type> {
    return this.http.get<Type>(environment.BASE_URL + api +'/' + id);
  }

  /**
   * Find all the elements
   * @returns gets the list of objects found
   */
  public findAll(params?: any): Observable<Type[]> {
    return this.http.get<Type[]>(environment.BASE_URL + api, { params });
  }

  /**
   * Delete an object by its identifier field
   * @param id the object identifier
   * @returns gets the response
   */
  public delete(id: any): Observable<Type> {
    return this.http.delete<Type>(environment.BASE_URL + api+'/' + id);
  }

  /**
   * Insert the data
   * @param data the object containing the data to be inserted
   * @returns gets the response
   */
  public insert(data: Type): Observable<Type> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<Type>(environment.BASE_URL + api, data, { headers });
  }

  /**
   * Update specific object into DB
   * @param Type the object to be updated
   * @returns gets the response
   */
  public update(data: Type): Observable<Type> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<Type>(environment.BASE_URL + api, data);
  }
}
