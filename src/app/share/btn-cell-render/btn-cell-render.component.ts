import { Component, OnDestroy, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams } from 'ag-grid-community';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-btn-cell-render',
  templateUrl: './btn-cell-render.component.html',
})
export class BtnCellRenderComponent implements ICellRendererAngularComp, OnInit, OnDestroy {
  constructor(private nzMessageService: NzMessageService) {}

  params: any;

  refresh(params: any): boolean {
    throw new Error('Method not implemented.');
  }

  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {}

  agInit(params: any): void {
    this.params = params;
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }
  btnMetaDatConfigClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.configClicked(this.params.data);
  }

  btnInfoClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.infoClicked(this.params.data);
  }

  btnEditClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.editClicked(this.params.data);
  }

  btnDeleteClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.deleteClicked(this.params.data);
  }
  btnprintClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.printClicked(this.params.data);
  }
  btnEnterFormulaClickedHandler($event: any): any {
  //console.log(" demo ",this.params.data);
    this.params.enterFormulaClicked(this.params.data);
  }

  btnAddClickedHandler($event: any): any {
   //console.log(" demo ",this.params.data);
    this.params.addClicked(this.params.data);
  }

  ngOnDestroy(): any {
    // no need to remove the button click handler
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }
}
