import { ElementRef, ViewChild } from '@angular/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AccountService } from 'src/app/core/services';
import { CartService } from 'src/app/core/services/Cart.service';
import { ProductService } from 'src/app/core/services/products.service';
import { v4 as uuidv4 } from 'uuid';
// const { v4: uuidv4 } = require('uuid');


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit {
  @ViewChild('closeBtn', { static: true }) closeBtn!: ElementRef;
  value: any = ""
  search?: any;
  optionGroups: any[] = [];
  options: string[] = [];
  loginForm!: FormGroup;
  rgForm!: FormGroup;
  forgotForm!: FormGroup;
  updateForm!: FormGroup;
  islogin = localStorage.getItem('isLogin')
  user: any;
  mess: any = "";
  isLoadingOne:boolean=false
  constructor(
    private productService: ProductService,
    private cartService: CartService,
    private fb: FormBuilder,
    private acc: AccountService,
    private msg: NzMessageService,
    private route: Router
  ) {
    const useId = 1
    if (!localStorage.getItem("user_id")) {
      localStorage.setItem("user_id", '' + useId)

    }
    if (!localStorage.getItem("cart_id")) {
      localStorage.setItem("cart_id", '')

    }
  }

  onInput(event: Event): void {
    this.getProducts()
  }

  ngOnInit(): void {
    if (this.islogin == "1") {
      let a = JSON.parse("" + localStorage.getItem('user-login'))
      this.user = a?.data
    }
    this.checkUserId()
    this.createFormLogin()
    this.createFormResgiter()
    this.createFormforgot()
    this.createFormUpdate()
  }
  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.rgForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };
  confirmValidatorchangepas = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.forgotForm.controls.pass_new.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  infor: any = {
    fullname: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    address: [null, [Validators.required]],
  }
  createFormResgiter() {
    this.rgForm = this.fb.group({
      ...this.infor,
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
      confirmpassword: [null, [this.confirmValidator]],

    });
  }
  createFormforgot() {
    this.forgotForm = this.fb.group({
      pass_old: [null, [Validators.required]],
      pass_new: [null, [Validators.required]],
      confirmpassword: [null, [this.confirmValidatorchangepas]],
    });
  }
  createFormUpdate() {
    this.updateForm = this.fb.group({
      ...this.infor,

    });
  }
  createFormLogin() {
    this.loginForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
    });
  }
  signup() {

    for (const i in this.rgForm.controls) {
      if (this.rgForm.controls.hasOwnProperty(i)) {
        this.rgForm.controls[i].markAsDirty();
        this.rgForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.rgForm.invalid) {
      this.mess = 'Vui lòng kiểm tra lại thông tin!'
      return;
    }
    this.isLoadingOne=true
    this.mess=""
    this.acc.register(this.rgForm.value).subscribe((res: any) => {
      if (res.data) {

        this.msg.success("Vui lòng kiểm tra mail!")
         window.location.reload()
      } else {
        this.mess = 'Đăng ký không thành công vui lòng thử lại!'
      }
    }, (err: any) => {
      this.mess = 'Đăng ký không thành công vui lòng thử lại!'
      console.log("login faile=", err);
    },()=>{
      this.isLoadingOne=false
    })
  }
  login() {
    for (const i in this.loginForm.controls) {
      if (this.loginForm.controls.hasOwnProperty(i)) {
        this.loginForm.controls[i].markAsDirty();
        this.loginForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.loginForm.invalid) {
      this.mess = 'Vui lòng kiểm tra lại thông tin!'
      return;
    }
    this.isLoadingOne=true
    this.mess=""
    this.acc.login(this.loginForm.value).subscribe((res: any) => {
      if (res.data) {
        localStorage.setItem("user_id", '' + res.data.id)
        localStorage.setItem("isLogin", "" + 1)
        if (res.data.role.name == "ROLE_ADMIN") {
          this.route.navigateByUrl("/order");
          this.closeBtn.nativeElement.click();
        } else {
          window.location.reload()
        }
        this.msg.success("Chúc mừng quý khách đăng nhập thành công")
      }else{
        this.mess = 'Đăng nhập không thành công vui lòng thử lại!'
      }
    }, (err: any) => {
      this.mess = 'Đăng nhập không thành công vui lòng thử lại!'
      console.log("login faile=", err);
    },()=>{
      this.isLoadingOne=false
    })
  }
  changepass() {
    for (const i in this.forgotForm.controls) {
      if (this.forgotForm.controls.hasOwnProperty(i)) {
        this.forgotForm.controls[i].markAsDirty();
        this.forgotForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.forgotForm.invalid) {
      this.mess = 'Vui lòng kiểm tra lại thông tin!'
      return;
    }
    this.isLoadingOne=true
    this.mess=""
    this.acc.changepass({ id: +this.user.id, ...this.forgotForm.value }).subscribe((res: any) => {
      if (res.data) {
        this.mess = "Đổi mật khẩu thành công"
      } else {
        this.mess = 'Đổi mật khẩu không thành công vui lòng thử lại!'
      }

    }, (err: any) => {
        this.mess = 'Đổi mật khẩu không thành công vui lòng thử lại!'
      console.log("login faile=", err);
    },()=>{
      this.isLoadingOne=false
    })
  }
  updateProfile() {
    for (const i in this.updateForm.controls) {
      if (this.updateForm.controls.hasOwnProperty(i)) {
        this.updateForm.controls[i].markAsDirty();
        this.updateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.updateForm.invalid) {
      this.mess = 'Vui lòng kiểm tra lại thông tin!'
      return;
    }
    this.isLoadingOne=true
    this.mess=""
    this.acc.updateProfile({ id: +this.user.id, ...this.updateForm.value }).subscribe((res: any) => {
      if (res.data) {
        console.log(res.data);
        let a = JSON.parse("" + localStorage.getItem('user-login'))
        const token = a?.token
        localStorage.setItem('user-login', JSON.stringify({ token: token, data: res.data }));
        this.mess = "Cập nhật thông tin cá nhân thành công"
        window.location.reload()
      } else {
        this.mess = 'Cập nhật thông tin cá nhân không thành công vui lòng thử lại!'
      }
    }, (err: any) => {
      this.mess = "Cập nhật thông tin cá nhân không thành công vui lòng thử lại!"
      console.log("login faile=", err);
    },()=>{
      this.isLoadingOne=false
    })
  }
  logout() {
    this.acc.logout()
  }
  getProducts() {
    this.productService.findAll().subscribe((data: any) => {
      if (this.search != " " || this.search != "" || this.search != null || this.search != undefined) {
        this.optionGroups = data.data
      }
    }, (err: any) => {
      console.log(err);

    })
  }
  checkUserId() {
    let id = localStorage.getItem("user_id") ? localStorage.getItem("user_id") : -1
    this.cartService.checkUserId(id).subscribe((res: any) => {
      if (!res.data) {
        this.autoCreateCart()
      } else {
        localStorage.setItem("cart_id", res.data.id)
      }
    }, (err: any) => {
      console.log(err);
    })
  }
  autoCreateCart() {
    const model = {
      user_id: localStorage.getItem("user_id"),
      payment_id: null
    }
    this.cartService.createCart(model).subscribe((res: any) => {
      localStorage.setItem("cart_id", res.data.id)
    }, (err: any) => {
      console.log(err);
    })
  }
}
