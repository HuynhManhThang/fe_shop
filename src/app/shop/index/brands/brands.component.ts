import { Component, OnInit } from '@angular/core';
import { BrandsService } from 'src/app/core/services/brand.service';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.less']
})
export class BrandsComponent implements OnInit {
  getData: any;
  constructor(private brandsService: BrandsService) { }
  array = [1, 2, 3, 4];
  ngOnInit(): void {
    this.getBrands()
  }

  getBrands() {
    this.brandsService.findAll().subscribe((data: any) => {
      this.getData = [data.data.slice(0,5),data.data.slice(5,10)]
    }, (err: any) => {
      console.log(err);

    })
  }
}
