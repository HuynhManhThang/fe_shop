import { NgModule } from '@angular/core';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { IndexRoutingModule } from './index-routing.module';

@NgModule({
  imports: [
    IndexRoutingModule,
    NzLayoutModule,
    IconsProviderModule,
  ],
  declarations: [ ],
})
export class IndexModule {}
