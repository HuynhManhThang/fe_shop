import { filter } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/core/services/order.service';

@Component({
  selector: 'app-check-order',
  templateUrl: './check-order.component.html',
  styleUrls: ['./check-order.component.less']
})
export class CheckOrderComponent implements OnInit {

  constructor(
    private myorder: OrderService
  ) { }
  datashow: any
  listOrder: any
  ngOnInit() {
    const data = JSON.parse("" + localStorage.getItem('user-login'))
    this.myorder.myorder(data.data.id).subscribe((res: any) => {
      this.listOrder = res.data;
      this.onIndexChange(0)
    }, (err: any) => {
      console.log(err)
    })
  }
  current = 0;
  onIndexChange(index: number): void {
    this.current = index;
    const fillterdata = this.listOrder.filter((item: any) => {
      return item.status_bill == this.current
    })
    var a: any[] = []
    fillterdata.map((item: any) => {
      const datapro = [...item.cart.productCarts]
      a = [...datapro, ...a]
    })
    this.datashow = a
  }

}
