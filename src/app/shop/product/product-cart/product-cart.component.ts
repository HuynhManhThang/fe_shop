import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CartService } from 'src/app/core/services/Cart.service';

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.less']
})
export class ProductCartComponent implements OnInit {
  myOrderItems: any;
  totalAmmount: any;
  isLoading: boolean = false;
  itemindex: any;
  check_cart_or_order: any
  checkoutForm!: FormGroup;
  submitted: boolean = false;
  isLoadingOne: boolean = false;
  islogin = localStorage.getItem('isLogin')
  user: any = null;
  patment:any
  constructor(
    private cartService: CartService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private msg:NzMessageService
  ) {
    this.check_cart_or_order = this.route.snapshot.routeConfig?.path == "cart" ? 1 : 0;
    this.checkoutForm = this.formBuilder.group({
      fullname: ["", Validators.required],
      phone: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      address: ["", Validators.required],
      cart_id: localStorage.getItem("cart_id")
    })
  }

  ngOnInit() {
    if (this.islogin == "1") {
      let a = JSON.parse("" + localStorage.getItem('user-login'))
      this.user = a?.data
      if (this.check_cart_or_order==0) {
        this.checkoutForm.controls['fullname'].setValue(this.user?.fullname)
        this.checkoutForm.controls['address'].setValue(this.user?.address)
        this.checkoutForm.controls['email'].setValue(this.user?.email)
        this.checkoutForm.controls['phone'].setValue(this.user?.phone)
      }

    }

    this.getCartById()
  }
  get f() { return this.checkoutForm.controls; }
  getCartById() {
    let id = localStorage.getItem("cart_id") ? localStorage.getItem("cart_id") : -1
    this.cartService.getCartById(id).subscribe((res: any) => {
      this.myOrderItems = res.data?.productCarts;
      this.getTotalPrice()
    }, (err: any) => {
      console.log(err);
    })
  }
  // Remove item from cart list
  removeItemFromCart(productId: any) {
    this.cartService.removeProductFromCart(productId).subscribe((res: any) => {
      this.getCartById()
    }, (err: any) => {
      console.log(err);

    });

  }
  updatePro(product: any) {
    this.isLoading = true;
    this.itemindex = product.id;
    const data = { productCart_id: product.id, quantity: product.quantity }
    this.cartService.update(data).subscribe((res: any) => {
      this.getCartById()
    }, (err: any) => {
      console.log(err);
    }, () => {
      this.isLoading = false
    });
  }
  getTotalPrice() {
    let total = 0;
    this.myOrderItems?.map((item: any) => {
      total += item.total_item;
    });
    this.totalAmmount = total
  }
  checkout() {
    this.submitted = true;
    this.router.navigateByUrl("/checkout")
    // stop here if form is invalid
    if (this.checkoutForm.invalid) {
      return;
    }
    if(this.patment&&this.totalAmmount!=0){
       this.isLoadingOne=true
    this.cartService.Bill({payment_id:this.patment,...this.checkoutForm.value}).subscribe((res: any) => {
      this.getCartById()
      localStorage.removeItem("cart_id");
      this.check_cart_or_order = 2,
    setTimeout(()=>{
      window.location.reload()
      this.router.navigateByUrl('/products')
    },2000)
    }, (err: any) => {
    }, () => {
      this.isLoading = false
      this.isLoadingOne=false
    });
    }else{
      this.msg.warning('Vui lòng chọn hình thức thanh toán hoặc giỏ hàng chưa có sản phẩm!')
    }
  }
}
