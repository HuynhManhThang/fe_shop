import { map } from 'rxjs/operators';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzImageService } from 'ng-zorro-antd/image';
import { ProductService } from 'src/app/core/services/products.service';
import { CartService } from 'src/app/core/services/Cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.less']
})
export class ProductDetailComponent implements OnInit,OnChanges {
  id: number | any;
  dataProduct: any;
  image: any[]=[];
  imageItem:any;
  constructor(private nzImageService: NzImageService,
    private route: Router,
    private router: ActivatedRoute,
    private cartService: CartService,
    private productService: ProductService
  ) {
    this.id = this.router.snapshot.params.id
    this.productService.findById(this.id).subscribe((res: any) => {
      console.log(res);
      if (res.data) {
        this.dataProduct = res.data;
        let images = this.dataProduct.images.split(",")
        images.map((item: any) => {
          this.image = [{
            src: item,
            alt: "H2N"
          }, ...this.image]
        })
        this.imageItem=this.image[0]?.src
      }

    }, (err) => {
      console.log(err);

    })
  }
  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges): void {
debugger
  }
  onClick(): void {
    this.nzImageService.preview(this.image, { nzZoom: 1.5, nzRotate: 0 });
  }
  addToCart(item:any,page?:any) {
    if (page==1) {
      this.route.navigateByUrl("/checkout")
      this.cartService.addProductToCart(item);
    }else{
      this.cartService.addProductToCart(item);
    }
  }
  changeImgae(img:any){
this.imageItem=img
  }
}
