import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/core/services/Cart.service';
import { ProductService } from 'src/app/core/services/products.service';
import {
  NzSkeletonAvatarShape,
  NzSkeletonAvatarSize,
  NzSkeletonButtonShape,
  NzSkeletonButtonSize,
  NzSkeletonInputSize
} from 'ng-zorro-antd/skeleton';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.less']
})
export class ProductItemComponent implements OnInit {
  p: number = 1;
  buttonActive = false;
  avatarActive = false;
  inputActive = false;
  imageActive = false;
  buttonSize: NzSkeletonButtonSize = 'default';
  avatarSize: NzSkeletonAvatarSize = 'default';
  inputSize: NzSkeletonInputSize = 'default';
  elementActive = true;
  buttonShape: NzSkeletonButtonShape = 'default';
  avatarShape: NzSkeletonAvatarShape = 'circle';
  elementSize: NzSkeletonInputSize = 'default';
  isloading: boolean = false
  @Input() sizeList = 0
  getData: any
  @Input() products: any = [];
  current = 1;
  hPage: boolean = false;


  constructor(private productService: ProductService,
    private route: Router,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.getProducts()
    this.sort()
  }

  getProducts() {
    this.isloading = true
    this.productService.findAll().subscribe((data: any) => {
      this.isloading = false
      if (this.sizeList == 0) {
        this.getData = data.data
        this.getData.sort(() => Math.random() - 0.5)
      } else {
        this.hPage = true
        this.getData = data.data.slice(0, this.sizeList)
        this.getData.sort(() => Math.random() - 0.5)
      }
    }, (err: any) => {
      console.log(err);

    })
  }

  addToCart(item: any, page?: any) {
    if (page == 1) {
      this.route.navigateByUrl("/checkout")
      this.cartService.addProductToCart(item);
    } else {
      this.cartService.addProductToCart(item);
    }
  }
  onRouter(id: any) {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.route.navigateByUrl("/product/" + id)
  }
  sort() {
    this.cartService.sharedData.subscribe((res: any) => {
      if (res != "") {
        if (res == 0) {
          this.getData = this.getData.sort((a: any, b: any) => {
            return a.price_new > b.price_new ? -1 : 1;
          });
        } else if (res == 1){
          this.getData = this.getData.sort((a: any, b: any) => {
            return a.price_new < b.price_new ? -1 : 1;
          });
        }else if(res == 3){
          this.getData.sort(() => Math.random() - 0.5)
        }
      }
    }, (err: any) => {
      console.log(err);
    })

  }
  fillter() {
    const object: any[] = []

    const data: any[] = []
    object.map((om) => {
      this.getData = this.getData.filter((res: any) => {
          return res.price_new == om
      })
    }
    )


  }
}
