import { ActivatedRoute } from '@angular/router';
import { TypesService } from './../../../core/services/type.service';
import { GendersService } from './../../../core/services/gender.service';
import { ColorsService } from './../../../core/services/color.service';
import { CartService } from 'src/app/core/services/Cart.service';
import { Component, OnInit } from '@angular/core';
import { BrandsService } from 'src/app/core/services/brand.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})
export class ProductListComponent implements OnInit {
  dataTile = [
    {
      title: "Đồng Hồ Nam",
      subTitle: "Mạnh mẽ, Phong độ, Lịch lãm"
    },
    {
      title: "Đồng Hồ Nữ",
      subTitle: "Phụ kiện mang lại sang trọng cho phái đẹp"
    },
    {
      title: "Đồng Hồ Cao cấp",
      subTitle: "Phong cách thượng lưu, Đỉnh cao danh vọng"
    }
    , {
      title: "Đồng Hồ H2N",
      subTitle: "Niềm tin và chất lượng"
    }
  ]
  ShowTitle: any
  constructor(
    private cartService: CartService,
    private colorService: ColorsService,
    private genderservice: GendersService,
    private typeService: TypesService,
    private brandService: BrandsService,
    private route: ActivatedRoute,

  ) {
    const url = this.route.snapshot.routeConfig?.path
    if (url == 'products/female') {
      this.ShowTitle = this.dataTile[1]
    } else if (url == 'products/male') {
      this.ShowTitle = this.dataTile[0]
    } else if (url == 'products/lux') {
      this.ShowTitle = this.dataTile[2]
    } else {
      this.ShowTitle = this.dataTile[3]
    }

  }

  ngOnInit(): void {
    this.binding()
  }
  selectsort: any = ""
  sortchange(id?: any) {
    if (id > 0) {
      this.cartService.setSort(id)
    } else {
 this.cartService.setSort(this.selectsort)
    }

  }
  reload(){
  window.location.reload()
  }
  typeList: any;
  brandList: any;
  colorList: any;
  genderList: any
  binding() {
    this.typeService.findAll().subscribe((data: any) => {
      if (data) {
        this.typeList = data.data.slice(0, 8)
      }
    })
    this.brandService.findAll().subscribe((data: any) => {
      if (data) {
        this.brandList = data.data.slice(0, 8)
      }
    })
    this.colorService.findAll().subscribe((data: any) => {
      if (data) {
        this.colorList = data.data.slice(0, 8)
      }
    })
    this.genderservice.findAll().subscribe((data: any) => {
      if (data) {
        this.genderList = data.data.slice(0, 8)
      }
    })
  }
}
