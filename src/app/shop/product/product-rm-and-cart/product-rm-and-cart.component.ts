import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CartService } from 'src/app/core/services/Cart.service';

@Component({
  selector: 'app-product-rm-and-cart',
  templateUrl: './product-rm-and-cart.component.html',
  styleUrls: ['./product-rm-and-cart.component.less']
})
export class ProductRmAndCartComponent implements OnInit {

  myOrderItems: any = null
  totalAmmount: any;

  private cartItems: any;

  constructor(
    private cartService: CartService,
    private msg: NzMessageService
  ) { }

  ngOnInit() {
    this.cartService.getProducts.subscribe((data: any) => {
      if (data) {
        this.myOrderItems = data;
        this.msg.success(`Thêm giỏ hàng thành công`)
        this.getTotalPrice()
      } else {
        this.getCartById()
      }
    });
    this.getCartById()
  }

  // Remove item from cart list
  removeItemFromCart(productId: any) {
    this.cartService.removeProductFromCart(productId);

  }
  getCartById() {
    let id = localStorage.getItem("cart_id") ? localStorage.getItem("cart_id") : -1
    this.cartService.getCartById(id).subscribe((res: any) => {
      this.myOrderItems = res.data?.productCarts;
      this.getTotalPrice()
    }, (err: any) => {
      console.log(err);

    })
  }
  getTotalPrice() {
    let total = 0;
    this.myOrderItems?.map((item: any) => {
      total += item.total_item;
    });
    this.totalAmmount = total
  }
}
