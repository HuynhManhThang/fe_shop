import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeProductComponent } from './index/home-product/home-product.component';
import { IndexComponent } from './index/base-index/index.component';
import { ProductCartComponent } from './product/product-cart/product-cart.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { CheckOrderComponent } from './product/check-order/check-order.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    // canLoad: [JWTGuard],
    children: [
      { path: '', redirectTo: '/index', pathMatch: 'full' },
      { path: 'index', component: HomeProductComponent },
      { path: 'product/:id', component: ProductDetailComponent },
      { path: 'products', component: ProductListComponent },
      { path: 'product', component: ProductListComponent },
      { path: 'products/male', component: ProductListComponent },
      { path: 'products/female', component: ProductListComponent },
      { path: 'products/lux', component: ProductListComponent },
      { path: 'cart', component: ProductCartComponent },
      { path: 'cart/:id', component: ProductCartComponent },
      { path: 'checkout', component: ProductCartComponent },
      { path: 'my-order', component: CheckOrderComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
