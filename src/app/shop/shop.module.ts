import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Component
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { ShopRoutingModule } from './shop-routing.module';
import { IndexComponent } from './index/base-index/index.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductItemComponent } from './product/product-item/product-item.component';
import { PortfolioComponent } from './share-component/portfolio/portfolio.component';
import { BannerComponent } from './share-component/banner/banner.component';
import { TestimonialsComponent } from './share-component/testimonials/testimonials.component';
import { ProductCategoryComponent } from './share-component/product-category/product-category.component';
import { HomeProductComponent } from './index/home-product/home-product.component';
import { ProductCartComponent } from './product/product-cart/product-cart.component';
import { NotFoundComponent } from '../share/not-found/not-found.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NzSpaceModule } from 'ng-zorro-antd/space';
// NG Zoro Template
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { ProductRmAndCartComponent } from './product/product-rm-and-cart/product-rm-and-cart.component';
import { BrandsComponent } from './index/brands/brands.component';
import { CommonModule } from '@angular/common';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NgxPaginationModule } from 'ngx-pagination';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { CheckOrderComponent } from './product/check-order/check-order.component';
@NgModule({
  imports: [
    CommonModule,
    ShopRoutingModule,
    NzLayoutModule,
    IconsProviderModule,
    NzMenuModule,
    NzCarouselModule,
    NzBackTopModule,
    NzImageModule,
    NzAutocompleteModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzEmptyModule,
    NzMessageModule,
    NzPaginationModule,
    Ng2SearchPipeModule,
    NzSkeletonModule,
    NzSpaceModule,
    NzBadgeModule,
    NzResultModule,
    NzStepsModule,
    NgxPaginationModule ,
    NzDropDownModule ],
  declarations: [
    IndexComponent,
    ProductListComponent,
    ProductItemComponent,
    BannerComponent,
    PortfolioComponent,
    TestimonialsComponent,
    ProductCategoryComponent,
    HomeProductComponent,
    ProductCartComponent,
    NotFoundComponent,
    ProductDetailComponent,
    ProductRmAndCartComponent,
    BrandsComponent,
    CheckOrderComponent
  ],
})
export class ShopModule {}
