// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // BASE_URL:'http://localhost:8080/',
  BASE_URL:'https://api-shopwatch.herokuapp.com/',
  firebaseConfig :{
    apiKey: "AIzaSyAvxIzIzc-U3qJnziwQWnQZPL5Nt1ks-Zc",
    authDomain: "watchshop-44.firebaseapp.com",
    projectId: "watchshop-44",
    storageBucket: "watchshop-44.appspot.com",
    messagingSenderId: "4210748151",
    appId: "1:4210748151:web:04ecdafed798a62ca541f2",
    measurementId: "G-E5P75STY6N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
